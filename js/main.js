window.Adaptativa = window.Adaptativa || (function() {
  var adaptativa = $('#adaptativa');
  adaptativa.on('click', '.adaptativa__card', handleAdaptativaCard);
  adaptativa.on('click', '.adaptativa__tile', handleAdaptativaTile);
  adaptativa.on('click', '.adaptativa__quiz__btn', handleAdaptativaQuizBtn);
  adaptativa.on('click', '.adaptativa__quiz__result__actions__btn-view', handleAdaptativaQuizViewBtn);
  adaptativa.on('click', '.adaptativa__btn-close', handleBtnClose);
  adaptativa.find('.adaptativa__quiz__modal-exit-quiz__box').on('click', 'button', handleBoxConfirmBtn);
  adaptativa.find('.adaptativa__combos__btn').on('click', handleBtnHeaderClick);

  function handleAdaptativaCard() {
    $('.adaptativa__materias').show();
  }

  function handleAdaptativaTile() {
    $('.adaptativa__quiz')
                    .show()
                    .find('.adaptativa__quiz__questions')
                    .show()
                    .end()
                    .find('.adaptativa__quiz__result')
                    .hide();
  }

  function handleAdaptativaQuizBtn() {
    $('.adaptativa__quiz')
                    .show()
                    .find('.adaptativa__quiz__questions')
                    .hide()
                    .end()
                    .find('.adaptativa__quiz__result')
                    .show();
  }

  function handleAdaptativaQuizViewBtn() {
    $(this)
      .closest('.adaptativa__quiz__result__option__item')
      .nextAll('.adaptativa__quiz__result__option__view')
      .toggleClass('adaptativa__quiz__result__option__view--opened');
    $(this).toggleClass('adaptativa__quiz__result__actions__btn-view--active');
  }

  function handleBtnClose() {
    var target = $(this).data('target');
    if (target === 'confirm') {
      showModalExitQuiz('materias');
    } else {
      $(this).closest('.' + target).hide();  
    }
  }

  function handleBoxConfirmBtn() {
    var modal = $(this).closest('.adaptativa__quiz__modal-exit-quiz');
    var to = modal.data('to');
    modal.hide()
    if ($(this).hasClass('adaptativa__btn-default')) {
      if (to === 'inicio') {
        hideModalExitQuiz();
      } else {
        $(this).closest('.adaptativa__content').hide();
      }
    }
  }

  function handleBtnHeaderClick() {
    if ($('.adaptativa__quiz').is(':visible')) {
      showModalExitQuiz('inicio');
    } else {
      hideModalExitQuiz();
    }
  }

  function showModalExitQuiz(to) {
    $('.adaptativa__quiz__modal-exit-quiz').css('display', 'flex').data('to', to);
  }

  function hideModalExitQuiz() {
    $('.adaptativa__content:gt(0)').hide();
  }

});

$(function() {
  window.Adaptativa();
});